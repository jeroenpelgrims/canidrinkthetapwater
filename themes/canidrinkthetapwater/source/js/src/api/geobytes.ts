import { fetch, CountryInfo } from '.';

interface GeobytesResponse {
  geobytesinternet: string;
  geobytescode: string;
}

export async function getCountryInfo() {
  return fetch<GeobytesResponse>('http://gd.geobytes.com/GetCityDetails')
    .then(response => {
      return {
        countryCode: response.geobytesinternet.toLowerCase(),
        areaCode: response.geobytescode.toLowerCase()
      } as CountryInfo;
    });
}