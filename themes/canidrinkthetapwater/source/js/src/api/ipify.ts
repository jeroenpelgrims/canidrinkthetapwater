import { fetch } from '.';

interface IpifyResponse {
  ip: string;
}

export async function getIp() {
  const response = await fetch<IpifyResponse>('https://api.ipify.org?format=json');

  return response.ip;
}