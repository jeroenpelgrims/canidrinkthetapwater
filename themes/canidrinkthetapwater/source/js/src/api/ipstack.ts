import { fetch } from '.';

interface IpInfo {
  country_code: string;
  region_code: string;
}

export async function getIpInfo(ip: string) {
  const accessKey = 'd3bedd94c7020bc133605e0c02e88900';
  const url = `http://api.ipstack.com/${ip}?access_key=${accessKey}`;

  return await fetch<IpInfo>(url);
}