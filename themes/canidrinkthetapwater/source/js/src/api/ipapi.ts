import { fetch, CountryInfo } from '.';

interface GeobytesResponse {
  country: string;
  region_code: string;
}

export async function getCountryInfo() {
  return fetch<GeobytesResponse>('https://ipapi.co/json/')
    .then(response => {
      return {
        countryCode: response.country.toLowerCase(),
        areaCode: response.region_code.toLowerCase()
      } as CountryInfo;
    });
}