export async function fetch<TResponse>(
  url: Request | string,
  init: RequestInit = { mode: 'cors' }
) {
  const response = await window.fetch(url, init);

  if (!response.ok) {
    throw response;
  }

  return await response.json() as TResponse;
}

export interface CountryInfo {
  countryCode: string;
  areaCode: string | undefined;
}

import { getCountryInfo as ipApiGetCountryInfo } from './ipapi';

export async function getCountryInfo() {
  const response = await ipApiGetCountryInfo();
  response.areaCode = undefined; // currenty only supporting country level data
  return response;
}