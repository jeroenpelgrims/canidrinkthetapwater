import { getCountryInfo } from './api';

function showLoader(visible: boolean = true) {
  const main = document.querySelector("main")!;
  main.classList.toggle("hidden", visible);
}

async function redirectToCountryPage() {
  const countryInfo = await getCountryInfo();
  const areaCodePath = countryInfo.areaCode ? `/${countryInfo.areaCode}` : '';

  location.pathname = countryInfo.countryCode + areaCodePath;
}

window.onload = () => {
  if (location.pathname === '/') {
    showLoader();
    redirectToCountryPage()
      .catch(() => {
        // TODO: show country selector instead
        const main = document.querySelector("main")!;
        main.classList.add('hidden');
        document.getElementById('adblock-warning')!.classList.remove('hidden');
        showLoader(false);
      });
  } else {
    // init country selector
  }
};