const fs = require('fs');
const path = require('path');

const data = JSON.parse(fs.readFileSync('data.json').toString());

for (const name in data) {
    const code = data[name].toLowerCase();

    if (!fs.existsSync(code)) {
        fs.mkdirSync(code);
    }

    const filePath = path.join(code, 'index.md');
    if (!fs.existsSync(filePath)) {
        console.log(filePath);
        fs.writeFileSync(filePath, `---\ncode: ${code}\nname: ${name}\ncandrink: unknown\n---\n`);
    }
}
